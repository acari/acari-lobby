FROM node:6.7.0

EXPOSE 3002

ADD . /acari-lobby
WORKDIR /acari-lobby
RUN npm install
CMD npm start
