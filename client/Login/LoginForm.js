import React from 'react'
import 'whatwg-fetch'

class LoginForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      nickRequest: ''
    }
    this.submitNick = this.submitNick.bind(this)
    this.handleNickChange = this.handleNickChange.bind(this)
  }

  componentDidMount() {
    this.nameInput.focus()
  }

  render() {
    return (
      <div className="loginForm">
        <h1>Acari</h1>
        <form className="form-login nickInput" onSubmit={this.submitNick}>
          <div className="input-group">
            <input
              className="form-control"
              placeholder="Enter your nickname"
              ref={ (input) => { this.nameInput = input }}
              onChange={this.handleNickChange}/>
            <span className="input-group-btn">
              <button className="btn btn-sm btn-primary" type="submit">Chat!</button>
            </span>
          </div>
        </form>
      </div>
    )
  }

  handleNickChange(e) {
    this.setState({nickRequest: e.target.value})
  }

  submitNick(e) {
    e.preventDefault()
    fetch('/login', {
      method: 'POST',
      body: JSON.stringify({nick: this.state.nickRequest})
    }).then((response) => {
      return response.json()
    }).then((json) => {
      console.log('set nick to: "', json.currentNick, '"')
      this.props.onLogin(json.currentNick)
      return json
    })
  }

}

LoginForm.propTypes = {
  onLogin: React.PropTypes.func
}

export default LoginForm
