import React from 'react'

class MessageInput extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      outGoingMessage: ''
    }

    this.sendMessage = this.sendMessage.bind(this)
    this.handleMessageChange = this.handleMessageChange.bind(this)
  }

  componentDidMount() {
    this.messageInput.focus()
  }

  render() {
    return (
      <div className="messageInput">
        <form className="commentForm" onSubmit={this.sendMessage}>
          <div className="input-group">
            <input
              className="form-control messageInputField"
              onChange={this.handleMessageChange}
              placeholder="Write message"
              value={this.state.outGoingMessage}
              ref={ (input) => { this.messageInput = input }}
              />
            <span className="input-group-btn">
              <button className="btn btn-primary" type="submit">Send</button>
            </span>
          </div>
        </form>
      </div>
    )
  }

  handleMessageChange(e) {
    this.setState({
      outGoingMessage: e.target.value
    })
  }

  sendMessage(e) {
    e.preventDefault()
    const messageToSend = this.state.outGoingMessage
    if (messageToSend) {
      const socket = this.props.socket
      socket.emit('message', messageToSend)
      this.setState({
        outGoingMessage: ''
      })
    }
  }

}

MessageInput.propTypes = {
  nick: React.PropTypes.string,
  socket: React.PropTypes.object
}

export default MessageInput
