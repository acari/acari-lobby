import React from 'react'

class UserList extends React.Component {

  render() {
    return (
      <div className="userList">
        <h4>Users</h4>
        <div className="onlineClass currentUser">
          <pre>Online</pre>
          <span className="badge badge-success">{this.props.nick}</span>
          {(this.props.users).map((user) => {
            const isCurrentUser = user.nickName === this.props.nick
            if (user.online && !isCurrentUser) {
              return <span className='badge badge-primary' key={user.socketId}>{user.nickName}</span>
            }
          })}
          <pre></pre>
        </div>
        <div className="onlineClass offlineUsers">
          <pre>Offline</pre>
          {(this.props.users).map((user) => {
            const isCurrentUser = user.nickName === this.props.nick
            if (!user.online && !isCurrentUser) {
              return <span className='badge badge-default' key={user.socketId}>{user.nickName}</span>
            }
          })}
          <pre></pre>
        </div>
      </div>
    )
  }
}

UserList.propTypes = {
  users: React.PropTypes.array,
  nick: React.PropTypes.string
}

export default UserList
