import Hapi from 'hapi'
import Path from 'path'
import inert from 'inert'
import Socketeer from './Socketeer'

const server = new Hapi.Server({
  connections: {
    routes: {
      files: {
        relativeTo: Path.join(__dirname, '../public')
      }
    }
  }
})

server.connection({ port: 3000 })

console.log('creating socket server')
const socketeer = new Socketeer(server)
console.log('sockets setup, users:', socketeer.users.length, 'messages:', socketeer.messages.length)

server.route({
  method: 'POST',
  path: '/login',
  handler: (request, reply) => {
    const payload = JSON.parse(request.payload)
    console.log('Request is:', payload)
    const responseObject = { 'currentNick': payload.nick }
    reply(responseObject)
  }
})

server.register(inert, (err) => {
  if (err) {
    throw err
  }

  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: '.'
      }
    }
  })
})

server.start((err) => {
  if (err) {
    throw err
  }
  console.log(`Server running at: ${server.info.uri}`)
})
