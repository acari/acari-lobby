import socketio from 'socket.io'
import DBAPI from './dbapi'

class Socketeer {
  constructor(server) {
    this.server = server
    this.dbapi = new DBAPI(this)
    this.io = socketio(server.listener)
    this.defineSockets(this.io)

    this.messages = []
    this.users = []
    this.localusers = []
  }

  defineSockets(io) {
    io.on('connection', (socket) => {
      socket.on('nick', (data) => {
        this.addUser(socket, data)
      })

      socket.on('users', (data) => {
        console.log('Users requested')
        socket.emit('users', this.users)
      })

      socket.on('messages', (data) => {
        console.log('messages requested')
        socket.emit('messages', this.messages)
      })

      socket.on('message', (data) => {
        console.log('Adding message', data)
        this.addMessage(socket, data)
      })

      socket.on('pingserver', (data) => {
        console.log('ping:', data)
      })

      socket.on('disconnect', (data) => {
      })

      socket.on('imhere', (data) => {
        console.log('user responded!', data)
        this.refreshUser(socket.id, data)
      })
    })
  }

  addUser(socket, data) {
    console.log('nick request: ', data)
    const userData = {
      socketId: socket.id,
      nickName: data
    }
    this.dbapi.userJoined(userData)
  }

  removeUser(socket, data) {
  }

  updateUserList(data) {
    console.log('Should update users - ', data.length)
    this.users = data
    this.io.sockets.emit('users', this.users)
  }

  refreshUsers() {
    console.log('Local users: ', this.localusers)
    const nickList = this.localusers.map((user) => { return user.nickName })
    console.log('Refreshing users, Currently on this server', nickList)

    var expiredUsers = []
    this.localusers.forEach((value, key, map) => {
      if (value.lastSeen < Date.now() - 10000) {
        expiredUsers.push(value)
      }
    })

    expiredUsers.forEach(expired => {
      console.log('REMOVING USER THAT LEFT', expired.nickName)
      this.removeUser(expired.socketId)
      this.localusers.splice(this.localusers.indexOf(expired), 1)
    })

    this.dbapi.reportLocalUsers(this.localusers)
    this.io.emit('users', this.users)

    // Send a 'ping' to all locally connected users
    this.io.sockets.emit('areyouthere')
  }

  refreshUser(socketId, nick) {
    var localUser = this.localusers.find((user) => {
      return user.socketId === socketId
    })

    if (!localUser) {
      this.localusers.push({nickName: nick, socketId: socketId, lastSeen: Date.now()})
    } else {
      localUser.lastSeen = Date.now()
    }
  }

  addMessage(socket, data) {
    const sender = this.localusers.find((user) => { return user.socketId === socket.id })
    const senderNick = sender ? sender.nickName : null

    if (!senderNick) {
      console.log('Unable to add message, user is perhaps still reconnecting?')
    } else {
      const messageData = {
        timestamp: 0 + (new Date()).getTime(),
        socketId: socket.id,
        nickName: senderNick,
        userId: 0,
        message: data
      }
      console.log('MESSAGE COUNT:', this.messages.length)
      this.dbapi.messageAdded(messageData)
    }
  }

  updateMessages(data) {
    console.log('Should update messages - ', data.length)
    this.messages = data
    this.io.emit('messages', this.messages)
  }

}

export default Socketeer
