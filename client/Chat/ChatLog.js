import React from 'react'

class ChatLog extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      room: 'General chat'
    }
  }

  componentWillReceiveProps(props) {
    this.chatLogElement.scrollTop = this.chatLogElement.scrollHeight
  }

  render() {
    const connectionStatus = this.props.connectionStatus ? 'Connected' : 'Disconnected'
    const connectionStyle = this.props.connectionStatus ? 'connectionStatus badge badge-success' : 'connectionStatus badge badge-danger'
    return (
      <div className="chatLog col-10"
        ref={ (div) => { this.chatLogElement = div }}
        >
        <h4 className="chatRoom">{this.state.room} -
          <span className={connectionStyle}>
            {connectionStatus}
          </span>
        </h4>
        <ul className="list messageList">
        {this.props.messages.map((message) => {
          const nickStyle = 'badge ' + (this.props.nick === message.nickName ? 'badge-success' : 'badge-default')
          const messageStyle = message.message.indexOf(this.props.nick) !== -1 ? 'message-highlight' : 'message'
          return (
            <li key={message.timestamp} className={messageStyle}>
              <span className={nickStyle}>
                {message.nickName}
              </span>
              <span className={messageStyle}>
                {message.message}
              </span>
            </li>
          )
        })}
        </ul>
      </div>
    )
  }

}

ChatLog.propTypes = {
  nick: React.PropTypes.string,
  messages: React.PropTypes.array,
  socket: React.PropTypes.object,
  connectionStatus: React.PropTypes.bool
}

export default ChatLog
