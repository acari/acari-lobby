import config  from './config'
import socketioclient from 'socket.io-client'

const dbapiurl = 'http://' + config.dbapi.url + ':' + config.dbapi.port

class DBAPI {
  constructor(socketeer) {
    this.socketeer = socketeer
    this.client = this.createClient()
    this.messageAdded = this.messageAdded.bind(this)
    this.userJoined = this.userJoined.bind(this)
    this.userLeft = this.userLeft.bind(this)
    this.retrieveUserList = this.retrieveUserList.bind(this)
    this.retrieveMessages = this.retrieveMessages.bind(this)
    this.receivedUserList = this.receivedUserList.bind(this)
    this.receivedMessages = this.receivedMessages.bind(this)
    this.refreshUsers = this.refreshUsers.bind(this)
  }

  createClient() {
    console.log('creating client')

    const client = socketioclient.connect(dbapiurl)

    client.on('connect', () => {
      console.log('wow, connected to DB API!')
      this.retrieveUserList()
      this.retrieveMessages()
    })

    client.on('userList', (data) => {
      console.log('Got new userList from DB API:', data.length, 'users')
      this.receivedUserList(data)
    })

    client.on('messageList', (data) => {
      console.log('Got messages from DB API:', data.length, 'messages')
      this.receivedMessages(data)
    })

    client.on('refreshUsers', () => {
      console.log('DB API Requesting user status refresh')
      this.refreshUsers()
    })

    console.log('client created')
    return client
  }

  retrieveUserList() {
    this.client.emit('users', 'retrieveUserList')
  }

  receivedUserList(data) {
    this.socketeer.updateUserList(data)
  }

  retrieveMessages() {
    this.client.emit('messages', 'retrieveMessages')
  }

  receivedMessages(data) {
    this.socketeer.updateMessages(data)
  }

  userJoined(userData) {
    this.client.emit('userJoined', userData)
  }

  userLeft(userData) {
    this.client.emit('userLeft', userData)
  }

  refreshUsers() {
    this.socketeer.refreshUsers()
  }

  reportLocalUsers(localusers) {
    this.client.emit('reportLocalUsers', localusers)
  }

  messageAdded(messageData) {
    this.client.emit('message', messageData)
  }

}

export default DBAPI
