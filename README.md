# Acari Lobby

## Running with docker compose (DEV)

Make sure you have run `docker-compose up` for the **[acari-db-api](https://bitbucket.org/acari/acari-db-api)** -project. The image is needed in the docker local repo to start up this project.

Start this project with `docker-compose up`.
All code changes will be immediately reflected inside the container and the server will restart.
