import React from 'react'
import io from 'socket.io-client'
const socket = io.connect()

import UserList from './UserList'
import ChatLog from './ChatLog'
import MessageInput from './MessageInput'

class Chat extends React.Component {
  constructor() {
    super()
    this.state = {
      connection: false,
      users: [],
      messages: []
    }

    socket.on('connect', (data) => {
      // This is fired on reconnection, since the socket has already connected.
      this.setState({connection: true})
    })

    socket.on('users', (data) => {
      this.setState({users: data})
    })

    socket.on('messages', (data) => {
      this.setState({messages: data})
    })

    socket.on('areyouthere', () => {
      socket.emit('imhere', this.props.nick)
    })

    socket.on('disconnect', (data) => {
      this.setState({connection: false})
    })
  }

  componentDidMount() {
    this.setState({connection: true})
    socket.emit('nick', this.props.nick)
    socket.emit('imhere', this.props.nick)
  }

  render() {
    return (
      <div className="chatContainer">
        <div className="chatMiddle">
          <div className="leftColumn">
            <ChatLog messages={this.state.messages} nick={this.props.nick} socket={socket} connectionStatus={this.state.connection}/>
          </div>
          <div className="rightColumn">
            <UserList users={this.state.users} nick={this.props.nick} socket={socket}/>
          </div>
        </div>
        <div className="chatBottom">
          <div>
            <MessageInput nick={this.props.nick} socket={socket} />
          </div>
        </div>
      </div>
    )
  }

}

Chat.propTypes = {
  nick: React.PropTypes.string
}

export default Chat
