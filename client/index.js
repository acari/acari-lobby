import React from 'react'
import Client from './Client'
import ReactDOM from 'react-dom'

ReactDOM.render((
  <Client />
), document.getElementById('app'))
