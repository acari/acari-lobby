import React from 'react'

import Chat from './Chat/Chat'
import LoginForm from './Login/LoginForm'

class Client extends React.Component {
  constructor() {
    super()
    this.state = {
      currentNick: ''
    }
    this.nickReceived = this.nickReceived.bind(this)
  }

  nickReceived(newNick) {
    this.setState({currentNick: newNick})
  }

  render() {
    if (this.state.currentNick) {
      return (
        <Chat className="chat" nick={this.state.currentNick} />
      )
    } else {
      return (
        <LoginForm onLogin={this.nickReceived} />
      )
    }
  }
}

export default Client
