export default {
  server: {
    port: 3002
  },
  dbapi: {
    url: 'localhost',
    port: 3001
  }
}
